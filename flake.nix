{
  description = "A flake for a pybatsim example";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/22.11";
    nur-kapack.url = "github:oar-team/nur-kapack";
    nur-kapack.inputs.nixpkgs.follows = "nixpkgs";
    batbroker.url = "git+https://gitlab.inria.fr/qguillot/batbroker";
    # batbroker.url = "/home/quentin/ghq/gitlab.inria.fr/qguillot/batbroker";
    qornflakes.url = "github:GuilloteauQ/qornflakes";
  };

  outputs = { self, nixpkgs, nur-kapack, batbroker, qornflakes }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
      lib = pkgs.lib;
      kapack = nur-kapack.packages.${system};
      batbroker-bin = batbroker.packages.${system}.batbroker;
      qorn = qornflakes.packages.${system};

      pname = "batcigri";
    in
    rec {
      packages.${system} = rec {
        default = self.packages.${system}.${pname};

        ${pname} = pkgs.python3Packages.buildPythonPackage rec {
          inherit pname;
          version = "0.0.0";
          format = "pyproject";
          src = ./batcigri;
          buildInputs = [ pkgs.poetry kapack.pybatsim-core-400 ];
          propagatedBuildInputs = [ kapack.pybatsim-core-400 ];
        };
      };

      devShells.${system} = {
        rshell = pkgs.mkShell {
          buildInputs = with pkgs; [
            R
            rPackages.tidyverse
            rPackages.geometry
            rPackages.fitdistrplus
            qorn.geomtextpath
          ];
        };
        
        paper = pkgs.mkShell {
          buildInputs = with pkgs; [
            pandoc
            texlive.combined.scheme-minimal
            rubber
          ];
        };

        default = pkgs.mkShell {
          buildInputs = with pkgs; [
            snakemake
            python3
            python3Packages.pandas
          ];
        };
        
        pyshell = pkgs.mkShell {
          buildInputs = with pkgs; [
            python3
          ];
        };
        
        debug = pkgs.mkShell {
          buildInputs = [
            kapack.batsim
            kapack.batexpe
            kapack.batsched
            kapack.pybatsim-core-400
            batbroker-bin
            pkgs.poetry
          ];
          shellHook = ''
            echo "batsim: $(batsim --version)"
            echo "simgrid: $(batsim --simgrid-version)"
            echo "robin: $(robin --version)"
            echo "pybatsim: $(pybatsim --version)"
          '';
        };

        batshell = pkgs.mkShell {
          buildInputs = [
            kapack.batsim
            kapack.batexpe
            kapack.batsched
            batbroker-bin
            self.packages.${system}.${pname}
            pkgs.poetry
          ];
        };
      };
    };
}
