#!/usr/bin/env bash

set -x

trap 'kill -TERM $PID1 $PID2 $PID3' TERM INT


function exit_properly {
    wait -n # Wait of one of the process (Requires bash>=4.3)
    exit_code=$?
    if [ $exit_code -ne 0 ]
    then
        >&2 echo subprocess failed with exit code: $exit_code
        kill -TERM $PID1 $PID2 $PID3
        exit $exit_code
    fi
    wait
}

log_file=$1
reference=$2
kp=$3
ki=$4

bat_endpoint="tcp://127.0.0.1:28000"
hpc_endpoint="tcp://127.0.0.1:28001"
bda_endpoint="tcp://127.0.0.1:28002"

batbroker -bat-endpoint $bat_endpoint -bda-endpoint $bda_endpoint -hpc-endpoint $hpc_endpoint &
PID1=$!
# run HPC scheduler
batsched -v conservative_bf -s $hpc_endpoint --verbosity quiet &
PID2=$!
# run BDA scheduler
pybatsim cigri_scheduler -s $bda_endpoint --scheduler-options "{\"log\": \"$log_file\", \"ref\": \"$reference\", \"kp\": \"$kp\", \"ki\": \"$ki\"}" &
PID3=$!


exit_properly
exit $exit_code
