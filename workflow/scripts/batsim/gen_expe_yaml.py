import argparse

def gen_config_batsim(inputs, output, ref, kp, ki, outfile):
    template = f"""
batcmd: >-
  batsim
  --enable-dynamic-jobs
  --acknowledge-dynamic-jobs
  --enable-profile-reuse
  -p {inputs}/platforms/cluster_q.xml
  -w {inputs}/workloads/hpc.json
  -w {inputs}/workloads/bda.json
  -e {output}/out
schedcmd: >-
  {inputs}/broker.sh
  {output}/cigri.csv
  {ref}
  {kp}
  {ki}
output-dir: {output}
simulation-timeout: 10
ready-timeout: 5
success-timeout: 5
failure-timeout: 0
"""
    with open(outfile, "w") as ofile:
        ofile.write(template)

def main():
    parser = argparse.ArgumentParser(description="Generate batsim expe.yaml for Robin")
    parser.add_argument('-r', '--ref', type=float, help='reference value')
    parser.add_argument('--kp', type=float, help='kp')
    parser.add_argument('--ki', type=float, help='ki')
    parser.add_argument('--inputs', help='directory of inputs')
    parser.add_argument('-o', '--output', help='output yaml')
    parser.add_argument('-d', '--output_directory', help='output directory')

    args = parser.parse_args()
    gen_config_batsim(args.inputs, args.output_directory, args.ref, args.kp, args.ki, args.output)
    
if __name__ == "__main__":
    main()
