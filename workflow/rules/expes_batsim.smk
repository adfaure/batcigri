scripts = config["scripts"]["batsim"]
workloads = config["workloads"]
inputs = config["inputs"]

rule generate_input_files_batsim:
  input:
    f"{scripts}/gen_expe_yaml.py"
  output:
    "data/batsim/inputs/expe_ref_{ref}_kp_{kp}_ki_{ki}.yaml"
  shell:
    f"nix develop .#pyshell --command python {{input}} -r {{wildcards.ref}} --kp {{wildcards.kp}} --ki {{wildcards.ki}} -o {{output}} -d data/batsim/outputs/expe_ref_{{wildcards.ref}}_kp_{{wildcards.kp}}_ki_{{wildcards.ki}} --inputs {inputs}"
    
rule run_batsim_expe:
  input:
    yaml="data/batsim/inputs/expe_ref_{ref}_kp_{kp}_ki_{ki}.yaml",
    wl_hpc=f"{workloads}/hpc.json",
    wl_cigri=f"{workloads}/bda.json",
    batcigri="batcigri/sched_cigri.py",
    controllers="batcigri/controllers.py"
  output:
    directory("data/batsim/outputs/expe_ref_{ref}_kp_{kp}_ki_{ki}")
  shell:
    "nix develop .#batshell --command robin {input.yaml}"
  
rule plot_batsim_results:
  input:
    data="data/batsim/outputs/expe_ref_{ref}_kp_{kp}_ki_{ki}",
    script=f"{scripts}/plot_batsim_result.R"
  output:
    "data/batsim/plots/expe_ref_{ref}_kp_{kp}_ki_{ki}.pdf"
  shell:
    "nix develop .#rshell --command Rscript {input.script} {input.data} {output}"