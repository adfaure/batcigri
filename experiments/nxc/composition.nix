{ pkgs, lib, modulesPath, flavour, nur, ... }: {
  nodes =
    let
      commonConfig =
        import ./common/common_config.nix { inherit pkgs lib modulesPath nur; };
      commonOARConfig =
        import ./common/common_oar_config.nix { inherit pkgs lib modulesPath nur; };
      nfsConfigs = import ./common/nfs.nix { inherit flavour; };
      scripts = import ./batsim-to-oarsub/script.nix { inherit pkgs; };
    in
    {
      oar-server = { pkgs, ... }: {
        imports = [ commonConfig commonOARConfig nfsConfigs.server ];
        environment.etc."oar/api-users" = {
          mode = "0644";
          text = ''
            user1:$apr1$yWaXLHPA$CeVYWXBqpPdN78e5FvbY3/
            user2:$apr1$qMikYseG$VL8nyeSSmxXNe3YDOiCwr1
          '';
        };
        services.oar.client.enable = true;
        services.oar.server.enable = true;
        services.oar.dbserver.enable = true;
        services.oar.web = {
          enable = true;
          extraConfig = ''
            #To support remote_ident custom header
            underscores_in_headers on;
          
            # location ^~ /api/ {
            #   rewrite ^/api/?(.*)$ /$1 break;
            #   include ${pkgs.nginx}/conf/uwsgi_params;
            #   uwsgi_pass unix:/run/uwsgi/oarapi.sock;
            # }

            location ^~ /oarapi-unsecure/ {
              rewrite ^/oarapi-unsecure/?(.*)$ /$1 break;
              include ${pkgs.nginx}/conf/uwsgi_params;
              uwsgi_pass unix:/run/uwsgi/oarapi.sock;
            }
          
          '';
        };
      };

      node = { pkgs, ... }: {
        imports = [ commonConfig commonOARConfig nfsConfigs.client ];
        services.oar.node = {
          enable = true;
          # register = { enable = true; };
          # register = { enable = true; nbResources = "32"; };
        };
      };

      cigri = { pkgs, ... }: {
        imports = [ nur.repos.kapack.modules.cigri nur.repos.kapack.modules.my-startup commonConfig nfsConfigs.client ];
        services.cigri = {
          dbserver.enable = true;
          client.enable = true;
          database = {
            host = "cigri";
            passwordFile = ./common/cigri-dbpassword;
            package = nur.repos.kapack.postgresql;
            # package = pkgs.postgresql_9_6;
          };
          server = {
            enable = true;
            web.enable = true;
            host = "cigri";
            logfile = "/tmp/cigri.log";
          };
        };
        networking.hostName = "cigri";
        services.my-startup = {
          enable = true;
          path = with pkgs; [ nur.repos.kapack.cigri sudo postgresql ];
          script = ''
            # Waiting cigri database is ready
            until pg_isready -h cigri -p 5432 -U postgres
            do
              echo "Waiting for postgres"
              sleep 0.5;
            done

            until sudo -u postgres psql -lqt | cut -d \| -f 1 | grep -qw cigri
            do
              echo "Waiting for cigri db created"
              sleep 0.5
            done

            newcluster cluster_0 http://oar-server/oarapi-unsecure/ none fakeuser fakepasswd "" oar-server oar2_5 resource_id 1 ""
            # newcluster cluster_0 http://oar-server/api/ none fakeuser fakepasswd "" oar-server oar2_5 resource_id 1 ""
            systemctl restart cigri-server
          '';
        };
      };
    };

dockerPorts.oar-server = [ "8443:443" "8000:80" ];
  rolesDistribution = { node = 1; };
  testScript = ''
    node.execute("systemctl restart oar-node-register")
  '';
}
