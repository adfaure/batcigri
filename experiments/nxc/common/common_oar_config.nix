{ pkgs, modulesPath, lib, nur, ... }:
let
  scripts = import ./scripts.nix { inherit pkgs; };
in
 {
  imports = [ nur.repos.kapack.modules.oar ];
  environment.etc."oar-dbpassword".text = ''
    # DataBase user name
    DB_BASE_LOGIN="oar"
      
    # DataBase user password
    DB_BASE_PASSWD="oar"
    # DataBase read only user name
    DB_BASE_LOGIN_RO="oar_ro"
    # DataBase read only user password
    DB_BASE_PASSWD_RO="oar_ro" 
  '';
  services.oar = {
    # oar db passwords
    database = {
      host = "oar-server";
      passwordFile = "/etc/oar-dbpassword";
      initPath = [ pkgs.util-linux pkgs.gawk pkgs.jq scripts.add_resources];
      postInitCommands = scripts.oar_db_postInitCommands;
    };
    server.host = "oar-server";
    privateKeyFile = "/root/.ssh/id_rsa";
    publicKeyFile = "/root/.ssh/id_rsa.pub";
    # privateKeyFile = "/etc/privkey.snakeoil";
    # publicKeyFile = "/etc/pubkey.snakeoil";
  };
}
