import json
import argparse
from random import lognormvariate

MU = 1.044
SIGMA = 0.2700


def gen_job(i, profile="cigri"):
    return {
        "id": i,
        "profile": profile,
        "res": 1,
        "subtime": 0
    }
    
def generate_random_profiles():
    """
    Generate random profiles to simulate the overhead of OAR    
    """
    
def generate_overhead():
    return lognormvariate(MU, SIGMA)

def gen_workload(res, duration, nb_jobs, outfile, with_overhead=False):
    wl = {}
    overheads_profiles = {}    
    
    cigri_profile = lambda duration: {
        "command": "sleep",
        "delay": duration,
        "type": "delay"
    }
    
    wl["jobs"] = []
    wl["nb_res"] = res
    wl["profiles"] = {}
    wl["profiles"]["cigri"] = cigri_profile(duration)
    
    for i in range(nb_jobs):
        if with_overhead:
            overhead = int(generate_overhead())
            if overhead in overheads_profiles:
                profile = overheads_profiles[overhead]
                wl["jobs"].append(gen_job(i + 1, profile))
            else:
                new_profile = cigri_profile(duration + overhead)
                profile_name = f"cigri_{overhead}"
                overheads_profiles[overhead] = profile_name
                wl["profiles"][profile_name] = new_profile
        else:
            wl["jobs"].append(gen_job(i + 1, "cigri"))
        
    with open(outfile, "w") as f:
        json.dump(wl, f, indent=4, sort_keys=True)
        
    
    
def main():
    parser = argparse.ArgumentParser(prog="Gen CiGri Workloads for BatCiGri")
    parser.add_argument('-n', '--nb_jobs', type=int, required=True)
    parser.add_argument('-d', '--duration', type=float, required=True)
    parser.add_argument('-r', '--nb_res', type=int, required=True)
    parser.add_argument('-o', '--output', type=str, required=True)
    parser.add_argument('--with-oar-overhead', action='store_true')
    
    args = parser.parse_args()
    
    gen_workload(args.nb_res, args.duration, args.nb_jobs, args.output, args.with_oar_overhead)
    
    
if __name__ == "__main__":
    main()

