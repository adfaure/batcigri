"""
schedBebida
~~~~~~~~~~~

This scheduler is the implementation of the BigData scheduler for the
Bebida on batsim project.

It is a Simple fcfs algoritihm.

It take into account preemption by respounding to Add/Remove resource
events. It kills the jobs that are allocated to removed resources. It also
kill some jobs in the queue in order to re-schedule them on a larger set of
resources.

The Batsim job profile "parallel_homogeneous_total" or a sequence of that kind
of jobs are MANDATORY for this mechanism to work.

Also, the `--enable-dynamic-jobs` and `--acknowledge-dynamic-jobs` Batsim CLI
option MUST be set"""

import copy
import math
import random
from itertools import islice

from procset import ProcInt, ProcSet

from pybatsim.batsim.batsim import BatsimScheduler, Job

from controllers import ControllerPI


def sort_by_id(jobs):
    return sorted(jobs, key=lambda j: int(j.id.split("!")[1].split("#")[0]))

class CiGriScheduler(BatsimScheduler):
    def filter_jobs_by_state(self, state):
        return sort_by_id(
            [job for job in self.bs.jobs.values() if job.job_state == state]
        )

    def running_jobs(self):
        return self.filter_jobs_by_state(Job.State.RUNNING)
        
    def waiting_jobs(self):
        return self.filter_jobs_by_state(Job.State.IN_SUBMISSON)

    def to_schedule_jobs(self):
        if len(self.filter_jobs_by_state(Job.State.IN_SUBMISSON)) > 0:
            return []
        return self.filter_jobs_by_state(Job.State.SUBMITTED)

    def in_killing_jobs(self):
        return self.filter_jobs_by_state(Job.State.IN_KILLING)

    def allocate_first_fit_in_best_effort(self, job):
        """
        return the allocation with as much resources as possible up to
        the job's `requested_resources` number.
        return None if no resources at all are available.
        """
        self.logger.info("Try to allocate Job: {}".format(job.id))
        assert (
            job.allocation is None
        ), "Job allocation should be None and not {}".format(
            job.allocation
        )

        nb_found_resources = 0
        allocation = ProcSet()
        nb_resources_still_needed = job.requested_resources

        iter_intervals = (self.free_resources & self.available_resources).intervals()
        for curr_interval in iter_intervals:
            if len(allocation) >= job.requested_resources:
                break
            interval_size = len(curr_interval)
            self.logger.debug("Interval lookup: {}".format(curr_interval))

            if interval_size > nb_resources_still_needed:
                allocation.insert(
                    ProcInt(
                        inf=curr_interval.inf,
                        sup=(curr_interval.inf + nb_resources_still_needed - 1),

                        # inf=(curr_interval.sup - nb_resources_still_needed + 1),
                        # sup=curr_interval.sup,
                    )
                )
            elif interval_size == nb_resources_still_needed:
                allocation.insert(copy.deepcopy(curr_interval))
            elif interval_size < nb_resources_still_needed:
                allocation.insert(copy.deepcopy(curr_interval))
                nb_resources_still_needed = nb_resources_still_needed - interval_size

        if len(allocation) > 0:
            job.allocation = allocation
            job.state = Job.State.RUNNING

            # update free resources
            self.free_resources = self.free_resources - job.allocation

            self.logger.info("Allocation for job {}: {}".format(job.id, job.allocation))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.to_be_removed_resources = {}
        self.load_balanced_jobs = set()
        
        self.waiting_queue = []

        self.cigri_period = 30
        self.continue_cigri = True
        
        reference = float(self.options["ref"])
        kp = float(self.options["kp"])
        ki = float(self.options["ki"])
        self.controller = ControllerPI(reference, kp, ki)
       
        self.notify_already_send = False

        log_filename = self.options["log"]
        self.log_file = open(log_filename, "w")
        self.log_file.write("time, ref, running, waiting, u\n")
        
        self.delay = 0

    def onSimulationBegins(self):
        self.free_resources = ProcSet(*[res_id for res_id in self.bs.compute_resources.keys()])
        self.nb_total_resources = len(self.free_resources)
        self.available_resources = copy.deepcopy(self.free_resources)
        random.seed(0)
        self.bs.wake_me_up_at(self.bs.time() + 0.1)
                
    def onJobSubmission(self, job):
        pass

    def onJobCompletion(self, job):
        # If it is a job killed, resources were already removed
        # and we don't want other jobs to use these resources.
        # But, some resources of the allocation are not part of the removed
        # resources: we have to make it available
        if job.job_state == Job.State.COMPLETED_KILLED:
            to_add_resources = job.allocation & self.available_resources
            self.logger.debug("To add resources: {}".format(to_add_resources))
            self.free_resources = self.free_resources | to_add_resources
        else:
            # update free resources
            self.free_resources = self.free_resources | job.allocation
            self.load_balance_jobs()
            self.schedule()

    def onRemoveResources(self, resources):
        self.available_resources = self.available_resources - resources

        # find the list of jobs that are impacted
        # and kill all those jobs
        to_be_killed = []
        for job in self.running_jobs():
            if job.allocation & resources:
                to_be_killed.append(job)

        if len(to_be_killed) > 0:
            self.bs.kill_jobs(to_be_killed)

        # check that no job in Killing are still allocated to this resources
        # because some jobs can be already in killing before this function call
        self.logger.debug("Jobs that are in killing: {}".format(self.in_killing_jobs()))
        in_killing = self.in_killing_jobs()
        if not in_killing or all(
            [
                len(job.allocation & resources) == 0
                for job in in_killing
            ]
        ):
            # notify resources removed now
            self.bs.notify_resources_removed(resources)
        else:
            # keep track of resources to be removed that are from killed jobs
            # related to a previous event
            self.to_be_removed_resources[str(resources)] = [
                job
                for job in in_killing
                if len(job.allocation & resources) != 0
            ]

    def onAddResources(self, resources):
        assert (
            len(resources & ProcSet(*self.bs.storage_resources)) == 0
        ), "Resources to be added should not contain storage resources!"
        self.available_resources = self.available_resources | resources
        # add the resources
        self.free_resources = self.free_resources | resources

        self.load_balance_jobs()
        self.schedule()

    def load_balance_jobs(self):
        """
        find the list of jobs that need more resources
        kill jobs, so they will be resubmited taking free resources, until
        there is no more resources
        """
        free_resource_nb = len(self.free_resources)
        to_be_killed = []

        for job in self.running_jobs():
            wanted_resource_nb = job.requested_resources - len(job.allocation)
            if wanted_resource_nb > 0:
                to_be_killed.append(job)
                free_resource_nb = free_resource_nb - wanted_resource_nb
            if free_resource_nb <= 0:
                break
        if len(to_be_killed) > 0:
            self.bs.kill_jobs(to_be_killed)
            # mark those jobs in order to resubmit them without penalty
            self.load_balanced_jobs.update({job.id for job in to_be_killed})

    def onJobsKilled(self, jobs):
        # First notify that the resources are removed
        to_remove = []
        for resources, to_be_killed in self.to_be_removed_resources.items():
            if len(to_be_killed) > 0 and any([job in jobs for job in to_be_killed]):
                # Notify that the resources was removed
                self.bs.notify_resources_removed(ProcSet.from_str(resources))
                to_remove.append(resources)
                # Mark the resources as not available
                self.free_resources = self.free_resources - ProcSet.from_str(resources)
        # Clean structure
        for resources in to_remove:
            del self.to_be_removed_resources[resources]

        for old_job in jobs:
            self.bs.resubmit_job(old_job)

    def onDeadlock(self):
        pass

    def onRequestedCall(self):
        """
        CiGri main loop:
            - computes the number of jobs to submit
            - schedule the jobs
            - ask Batsim to be waking up later  
        """
        # Controller Part ----------------------------------------------------------------------
        occupied_resources = self.nb_total_resources - len(self.free_resources)
        sensor = len(self.waiting_queue) + occupied_resources
        # self.controller.update_error(occupied_resources)
        self.controller.update_error(sensor)
        self.controller.update_input()
        nb_resources_to_submit = self.controller.get_input()
        # --------------------------------------------------------------------------------------
        self.log_file.write(f"{self.bs.time()}, {self.controller.reference}, {occupied_resources}, {len(self.waiting_queue)} ,{nb_resources_to_submit}\n")
        self.add_to_waiting_queue(nb_resources_to_submit)

        to_schedule_jobs = self.to_schedule_jobs()
        if len(to_schedule_jobs) > 0:
            # as for CiGri we submit all the jobs at the beginning,
            # if we schedule all the jobs, there are no more to execute
            self.bs.wake_me_up_at(self.bs.time() + self.cigri_period)
        else:
            self.bs.notify_registration_finished()
            self.log_file.close()
            
    def add_to_waiting_queue(self, nb_resources_to_submit):
        to_schedule_jobs = self.to_schedule_jobs()
        nb_submitted_resources = 0
        for job in to_schedule_jobs:
            if nb_submitted_resources >= nb_resources_to_submit:
                break
            if job not in self.waiting_queue:
                self.waiting_queue.append(job)
                nb_submitted_resources += 1 #TODO

        self.schedule()


    def schedule(self):
        # Implement a simple FIFO scheduler
        if (len(self.free_resources & self.available_resources) == 0
                or len(self.load_balanced_jobs) != 0):
            return
        if self.delay < 3:
            self.delay += 1
            return
        to_execute = []
        to_schedule_jobs = self.waiting_queue
        self.logger.info(
            "Start scheduling jobs, nb jobs to schedule: {}".format(
                len(to_schedule_jobs)
            )
        )

        self.logger.debug("jobs to be scheduled: \n{}".format(to_schedule_jobs))


        for job in to_schedule_jobs:
            if len(self.free_resources & self.available_resources) == 0:
                break
            self.allocate_first_fit_in_best_effort(job)
            to_execute.append(job)
            self.waiting_queue.remove(job)

        self.bs.execute_jobs(to_execute)
        for job in to_execute:
            job.job_state = Job.State.RUNNING
        self.logger.info(
            "Finished scheduling jobs, nb jobs scheduled: {}".format(len(to_execute))
        )
        self.logger.debug("jobs to be executed: \n{}".format(to_execute))