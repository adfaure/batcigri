

class Controller:
    def __init__(self, reference=0):
        self.nb_jobs = 0
        self.error = 0
        self.cumulative_error = 0
        self.reference = reference
        
    def get_input(self):
        return self.nb_jobs
        
    def update_error(self, sensor):
        self.error = self.reference - sensor
        self.cumulative_error += self.error
        
    def _update_input(self):
        pass
        
    def update_input(self):
        self._update_input()
        self.nb_jobs = self.nb_jobs if self.nb_jobs > 0 else 0
        
        
class ControllerPI(Controller):
    def __init__(self, reference, kp, ki):
        super().__init__(reference)
        self.kp = kp
        self.ki = ki
        
    def _update_input(self):
        self.nb_jobs = self.kp * self.error + self.ki * self.cumulative_error# - 12.7

        
    
