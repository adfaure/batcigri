---
title: Simulating a Multi-Layered Grid Middleware
author:
  - name: "Quentin Guilloteau"
    affiliation: 1
affiliation:
  - id: 1
    institute: "Univ. Grenoble Alpes, Inria, CNRS, LIG, F-38000 Grenoble France"
    email: Firstname.Lastname@inria.fr
keywords:
  - Distributed Systems
  - Deployment
  - Folding  
bibliography: references.bib
geometry: margin=20mm
numbersections: yes
abstract: |
  The study of grid or cluster middlewares is complex, and experiments on such systems are costly.
  This cost can come from the number of resources required to deploy realistic experiments, or the time to replay a significative workload.
  Simulation techniques can help reduce such cost to almost none and help perform preliminary study at low cost, but they might also degrade the realism of the results.
  In this paper, we consider the implementation in simulation of the \cigri\ grid middleware, in \bat, a batch sheduler simulator.
  We are particularly interested in the impact of simulation on signals of interest and their dynamics compared to the real system in the optic of using this simulator to accelerate the first steps of future studies.
header-includes: |
  \usepackage{caption}
  \usepackage{subcaption}
  \usepackage{booktabs}
  \usepackage{longtable}
  \usepackage{multirow}
  \usepackage{hyperref}
  \usepackage{xcolor}
  \usepackage{listings}
  \usepackage{algorithm2e}
  \usepackage[inline]{enumitem}
  \newcommand{\io}{\emph{I/O}}
  \newcommand{\qos}{\emph{Quality-of-Service}}
  \newcommand{\repro}{reproducibility}
  \newcommand{\Repro}{Reproducibility}
  \newcommand{\transpo}{\emph{Transposition}}
  \newcommand{\flavour}{\emph{flavour}}
  \newcommand{\flavours}{\emph{flavours}}
  \newcommand{\ie}{\emph{i.e.,}}
  \newcommand{\eg}{\emph{e.g.,}}
  \newcommand{\nix}{\emph{Nix}}
  \newcommand{\oar}{\emph{OAR}}
  \newcommand{\cigri}{\emph{CiGri}}
  \newcommand{\nixos}{\emph{NixOS}}
  \newcommand{\nxc}{\emph{NixOS Compose}}
  \newcommand{\enos}{\emph{EnOSlib}}
  \newcommand{\grid}{\emph{Grid'5000}}
  \newcommand{\kam}{\emph{Kameleon}}
  \newcommand{\kad}{\emph{Kadeploy}}
  \newcommand{\mel}{\emph{Melissa}}
  \newcommand{\bat}{\emph{Batsim}}
  \newcommand{\batcigri}{\emph{BatCiGri}}
  \newcommand{\store}{\emph{Nix Store}}
  \newcommand\todo[1]{{\textcolor{red}{TODO: #1}}}
  \definecolor{commentcolour}{rgb}{0.04,0.43,0.17}
  \definecolor{keywordcolour}{rgb}{0.65,0.15,0.64}
  \definecolor{backcolour}{rgb}{1,1,1}
  \definecolor{linenumbercolour}{rgb}{0.1,0.1,0.1}
  \definecolor{stringcolour}{rgb}{0.56,0.06,0.49}
  \colorlet{punct}{red!60!black}
  \definecolor{delim}{RGB}{20,105,176}
  \colorlet{numb}{magenta!60!black}
  \lstdefinelanguage{json}{
    literate=
     *{0}{{{\color{numb}0}}}{1}
      {1}{{{\color{numb}1}}}{1}
      {2}{{{\color{numb}2}}}{1}
      {3}{{{\color{numb}3}}}{1}
      {4}{{{\color{numb}4}}}{1}
      {5}{{{\color{numb}5}}}{1}
      {6}{{{\color{numb}6}}}{1}
      {7}{{{\color{numb}7}}}{1}
      {8}{{{\color{numb}8}}}{1}
      {9}{{{\color{numb}9}}}{1}
      {:}{{{\color{punct}{:}}}}{1}
      {,}{{{\color{punct}{,}}}}{1}
      {\{}{{{\color{delim}{\{}}}}{1}
      {\}}{{{\color{delim}{\}}}}}{1}
      {[}{{{\color{delim}{[}}}}{1}
      {]}{{{\color{delim}{]}}}}{1},
  }
  \lstdefinestyle{mystyle}{
      backgroundcolor=\color{backcolour},   
      commentstyle=\color{commentcolour},
      keywordstyle=\color{keywordcolour},
      numberstyle=\tiny\color{linenumbercolour},
      stringstyle=\color{stringcolour},
      basicstyle=\ttfamily\footnotesize,
      breakatwhitespace=false,         
      breaklines=true,                 
      captionpos=b,                    
      keepspaces=true,                 
      numbers=left,                    
      numbersep=5pt,                  
      showspaces=false,                
      showstringspaces=false,
      showtabs=false,                  
      tabsize=2,
      framexrightmargin=-12pt
  }
  \lstset{style=mystyle, frame=single}
  \usepackage{tcolorbox}
  \tcbuselibrary{theorems}
  \usepackage{cleveref}

  \newtcbtheorem[]{lesson}{Take away \#}{colback=black!5,colframe=black!35,fonttitle=\bfseries}{th}
---

# Introduction

Distributed experiments are complex and often require several machines for several hours or days. 
Such experiments are time and resource consuming, but are nevertheless mandatory to validate research work.
Deploying and running long-lasting experiments during the exploring phases of research is an obstacle to careful and sane work and must be addressed.

Simulation techniques are an adequate solution as they allow users to execute in reasonable time and on a single laptop, experiments that would have taken hours on a production platform. 
In the context of High-Performance Computing (HPC), most of the effort in terms of simulators is focused on tools to evaluate scheduling algorithms\ \cite{dutot:hal-01333471, galleguillos2018accasim, klusavcek2020alea}.
These solutions reduce considerably the time and computing power required to replay long scientific workloads with a new scheduling strategy instead of deploying a modified batch scheduler and re-executing the jobs of the workload, but have limitations in terms of realism due the underlying models.

Experiments on systems such a grid or cluster middlewares are also victim of high experimental costs and could benefit from simulation techniques.
However, due to this additional layer, the simulators cited above are not directly equipped to simulate such systems.  

In this paper, we present and evaluate \batcigri, a simulator of the \cigri\ grid middleware within \bat.
Section \ref{sec:cigri} details the studied middleware and the desired properties of its simulation.
In Section \ref{sec:batcigri} we present the design of the simulation of the middleware as well as its calibration to better match reality.
The evaluation of the simulation is performed in Section \ref{sec:eval}. 



# Motivating Example: the \cigri\ middleware {#sec:cigri}

## Presentation

\cigri\ \cite{cigri} is a grid middleware in production at the French *Gricad* meso-center \footnote{\url{https://gricad.univ-grenoble-alpes.fr/index_en.html}}.
The goal of \cigri\ is to use the idle resources of the meso-center.
It interacts with several clusters managed by \oar\ \cite{capit_batch_2005} batch schedulers.

Users of \cigri\ submit *Bag-of-Tasks* applications to the middleware.
Such applications are composed of thousands of short, independent and similar tasks are classified as *embarrassingly parallel* which make them good candidates for "filling the holes" in the cluster schedules.
Monte-Carlo simulations or parameter sweeps are examples of *Bag-of-Tasks* applications.

Once the set of tasks submitted to \cigri, the middleware will submit sub sets of jobs to the different schedulers of the grid.
The jobs are submitted with the lowest priority (best-effort) in order to allow premium users of the clusters to get the resources used by \cigri\ jobs if needed.

\begin{figure}
  \centering
  \includegraphics[width = 0.45\textwidth]{./figs/schemas/cigri.png}
  \caption{%
  Interactions between \cigri\ and the schedulers \oar\ of the computing grid.
  \cigri\ users submit \textit{Bag-of-Tasks} applications, whose jobs are then submitted to the different cluster schedulers of the computing grid.
  The computing clusters are shared with users with more priority, thus \cigri\ jobs must be killed if one premium user requires the resources.}
  \label{fig:cigri}
\end{figure}

Figure \ref{fig:cigri} depicts the interactions between \cigri\ and different clusters of the grid.


## Limitation of \cigri

One problem of \cigri\ is its submission algorithm.
\cigri\ will submit a batch of jobs the one scheduler, and wait for the completion of the batch to submit again.
This strategy can lead to an underutilization of the cluster resources. 
For example, \cigri\ might wait for the very last job of the previous submitted batch to terminate while there would be plenty of idle resources.
Moreover, one objective of \cigri\ is to harvest in a *non-invasive* fashion.
Meaning that the premium users of the different clusters must not notice the impact of the \cigri\ jobs on the platform.
However, once executing, the \cigri\ jobs are using the shared resources of the cluster (file-system, network, etc.), which can have an impact on the performance of every other running jobs.

<!---
In the context of research projects requiring to modify \cigri\ to control its job submission based on the load of the cluster resources\ \cite{guilloteau:mfc, guilloteau:icstcc}, we must deploy and perform experimentation evaluation of a modified version of \cigri\ on a realistic environment.
---->

## Feedback Loop Regulation

We address these limitations of the current \cigri\ submission algorithm by considering the problem from the point of view of Autonomic Computing\ \cite{kephart2003vision}.
One aspect of Autonomic Computing is the self-regulation of systems, where the controlled systems is cyclically monitored via sensors, and then based on the sensors, the autonomic controller will act on the system to direct it towards a desired state.
The implementation of the decision process can be done via multiple techniques (IA, rules, modelisation and optimal solving, etc.).
But in our work\ \cite{guilloteau:icstcc, guilloteau:mfc}, we implement the autonomic controller with tools from Control Theory.
Control Theory is a field from physical engineering for the regulation of dynamic systems.
It has been used for centuries on physical systems, and its properties have been proven mathematically.
Its usage on computing systems is only recent.
To implement a controller with Control Theory tools, the definition of the signals as well as their dynamic must be clearly identified and modeled. 

To test our version of \cigri\ with our controllers, we deploy a modified \cigri\ as well as an instance of \oar\ and compute nodes.
In order to perform faithful evaluations, it is unreasonable to deploy on the *entire* *Gricad* meso-center, and replay long workloads.
We are thus interested in simulation techniques to reduce the experimental costs.

However, one potential limitation of using simulation techniques in our case, is the inability to obtain the same signals, or for the signals to have a different dynamic or properties. 


## Expected Properties of the Simulation

For the \cigri\ simulation to be useful from the point of view of Control Theory it must have the following properties:

- Jobs must have realistic execution times

- Best-effort jobs must be killed and release resources for the normal jobs

- The killing and releasing of the resources must happen in a realistic time 

- Information about the usage of the platform and about the inner state of the scheduler must be accessible


# \batcigri\ {#sec:batcigri}

In this Section, we present a solution based on \bat\ \cite{dutot:hal-01333471} to simulate \cigri: \batcigri.

## Hypotheses

We work under the following hypotheses: (*i*) there is only one cluster in the grid, and (*ii*) the only best-effort jobs come from \cigri.
Regular users of the cluster cannot submit best-effort jobs.

## \bat\ in a Nutshell

\bat\ is a batch scheduler simulator which allows users to test their scheduling algorithms, \ie\ how the jobs are mapped to the resources. 
\bat\ relies on *Simgrid*\ \cite{casanova:hal-01017319} for sound simulation models.

The remaining of this section presents two important concepts of \bat: *platforms* and *workloads*.

#### Platforms

\bat\ platforms, similarly to *Simgrid* platforms, contain information about the underlying platform of the simulation.
It contains the number of hosts, the network topology, the speed of the links, the capacity of the disks, etc. 

#### Workloads

Workloads contain information about the jobs that will participate in the simulation.
There are two main components: `jobs` and `profiles`.
Profiles define the behavior of the jobs, \ie\ the underlying simulation to use (delay, parallel tasks, SMPI, etc.), execution times, SMPI trace to replay, etc.
In a \bat\ workload, a job belongs to a profile.
Each job must have an identifier, a submission time and a requested number of resources. 
Listing\ \ref{lst:workload} shows a simple example of \bat\ workload.


A study of the \cigri\ jobs running on the *Gricad* platform \cite{guilloteau:cigri_jobs} gives a statistical description of the execution times of those jobs.
This allows us to use a delay model to represent the execution times.


\begin{figure}[htbp]
\begin{tabular}{p{0.5\textwidth}p{0.5\textwidth}}
    \begin{minipage}{.5\textwidth}
\begin{lstlisting}[language=json, caption={Example of \bat\ workload with 3 jobs belonging to the \texttt{cigri} profile. Each job requests one resource and are submitted at the start of the simulation.}, label=lst:workload ]
{
    "jobs": [
        {
            "id": 1,
            "profile": "cigri",
            "res": 1,
            "subtime": 0
        },
        {
            "id": 2,
            "profile": "cigri",
            "res": 1,
            "subtime": 0
        },
        {
            "id": 3,
            "profile": "cigri",
            "res": 1,
            "subtime": 0
        }
    ],
    "nb_res": 32,
    "profiles": {
        "cigri": {
            "delay": 235.0,
            "type": "delay"
        }
    }
}
\end{lstlisting}
\end{minipage}
    &
    \begin{minipage}{.5\textwidth}
      \centering
      \includegraphics[width = 0.9\textwidth]{figs/schemas/seq_diag.pdf}
\caption{
Sequence Diagram representing the killing of best-effort jobs when a new priority job is submitted, as well as when a priority job finishes making its resources idle and thus exploitable by \cigri.
}
      \label{fig:seq_diag}
    \end{minipage}
\end{tabular}
\end{figure}



## Two Schedulers

\cigri\ requires two levels of scheduling.
The first level is from \cigri\ to \oar for best-effort jobs, and then from \oar\ to the nodes for normal users.
Our simulation needs to capture these two levels.

To do so we will have two \bat\ schedulers: one for the \cigri\ jobs and one for the priority jobs.
Each scheduler will manage their own workload but will schedule on the same platform.

As best-effort jobs need to have less priority on the normal jobs, we need a way to kill them.
The \cigri\ scheduler will thus only see the free resources of the cluster to perform its schedule of best-effort jobs.
On the other hand, the priority scheduler do not see the resources taken by \cigri\ jobs as occupied, and can decide to schedule jobs on those resources.
In this case, the \cigri\ scheduler must manage the killing of its jobs.

To be as close to reality, we used the same scheduling algorithms as the real system: conservative backfilling for the priority jobs, and First-Come-First-Served (FCFS) for the \cigri\ jobs.

## Broker

\bat\ can only communicate with a single scheduler.
However, as seen in the previous section, we have two different schedulers.
To deal with this limitation, we used the work done in \cite{mercier_contribution_2019} which implements a message broker between \bat\ and the schedulers.

The two schedulers connect to the broker and the broker connects to \bat.
It filters and redirect the message between the different actors.
The main of the work is to manage adaptation of the available resources for the \cigri\ scheduler.
When a priority job is submitted, \bat\ sends a `JOB_SUBMITTED` message to the broker.
The broker will then forward this message to the priority job scheduler.
If the allocation of resources returned by the scheduler contains best-effort jobs, the broker will inform the \cigri\ scheduler by sending a `REMOVE_RESOURCES` message.   
In this case, the \cigri\ scheduler must take care of the killing of the concerned jobs and their resubmission in its queue.
When a priority job terminates, its resources become free and thus available to the \cigri\ scheduler.
Then, the broker will send a `ADD_RESOURCES` message to \cigri\ to indicate the availability of new resources.

Figure \ref{fig:seq_diag} depicts the sequence diagram of a killing of a best-effort job due to a submission of a normal job.


## The \cigri\ Submission Loop

By taking advantage of the `CALL_ME_LATER` event of \bat, we are able to simulate the cyclic behavior of \cigri.
At every cycle, the \cigri\ scheduler will read the value of the sensors, compute the control error, compute the number of jobs to submit and submit them.

In our case, the sensor is the number of best-effort resources in waiting queue and the number of resources used on the platform.
The length of the waiting queue is internal information for the scheduler, whereas the number of resources used is computed indirectly.
Remember that the \cigri\ scheduler only sees the resources that are not used by the priority scheduler.
Thus, the number of resources currently used on the cluster is the total number of resources minus the number of resources visible by \cigri\ and plus the number of resources used by \cigri\ jobs.

The remaining of the \cigri\ cycle is relatively straightforward and is shown in Listing \ref{lst:cigri}.
All the \cigri\ jobs are available at the start of the simulation.
This means that in the \bat\ workload, they are submitted at time 0.

## Workload Adjustments

The synchronization between the real experiments and the simulation is complex, and thus the simulation workload needs to be adjusted to match the real workload.

<!---
- when is start

- oar takes 3 iteration to start jobs

- (de)comission

- killing of jobs
--->

#### Starting Delay of \oar

Performed experiments showed that \oar\ needs about 1 minutes and 30 seconds to start the first jobs after the first submission. 
This delay should be taken into account in the simulation.
From the point of view of the \cigri\ scheduler, this delay can be approximated by not starting the jobs submitting from the first 3 \cigri\ cycles.

\begin{minipage}[h]{\linewidth}
\begin{lstlisting}[language=Python, caption={Implementation of the \cigri\ submission loop in \bat. It is triggered by the \texttt{CALL\_ME\_LATER} event. At the end of each loop, we ask \bat\ to notify us for the next loop (line 18).}, label=lst:cigri]
def onRequestedCall(self):
  # Controller Part ----------------------------------------------------------------------
  occupied_resources = self.nb_total_resources - len(self.free_resources)
  sensor = len(self.waiting_queue) + occupied_resources

  self.controller.update_error(sensor)
  self.controller.update_input()
  nb_resources_to_submit = self.controller.get_input()
  # --------------------------------------------------------------------------------------

  # Submission Part ----------------------------------------------------------------------
  self.add_to_waiting_queue(nb_resources_to_submit)
  to_schedule_jobs = self.to_schedule_jobs()
  # --------------------------------------------------------------------------------------

  if len(to_schedule_jobs) > 0:
      # Ask Batsim to notify for the next cycle
      self.bs.wake_me_up_at(self.bs.time() + self.cigri_period)
  else:
      self.bs.notify_registration_finished()
\end{lstlisting}
\end{minipage}



#### Commission and Decommission Times

Another source of divergence between simulation and real execution, is the commission and decommission of the resources by \oar.
This (de)commission time is required to set up the computing nodes for the starting jobs, and to clean the nodes after the termination of the jobs.
This delay is not present in \bat\ and must be considered for realism.
We evaluated the (de)commission overhead by submitting jobs which perform an identical and precise amount of work, and compare it to the execution time given by \oar\ (\ie\ termination time minus starting time).
Figure\ \ref{fig:com_decom} shows the distribution of overheads in seconds.
This distribution shows that the overheads are mostly about 2 or 3 seconds and that the distributions has a long tail.  
We performed a fitting of a Log-Normal law on the overheads' data to retrieve a statistical model.
The fitting yielded that the overheads follow a distribution $Lognormal(1.04, 0.27)$.
Figure\ \ref{fig:ecdf} shows the cumulative distribution functions of the overhead (solid line) and the model (dashed line). 
This model allows us to generate \bat\ workloads containing this overhead in the execution time of the jobs.

\begin{figure}
     \centering
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figs/graphs/histogram_overhead.pdf}
         \caption{Histogram of the distribution of job overhead due to the commission and decommission of resources by \oar. Most of the overhead is around 2 and 3 seconds.}
         \label{fig:com_decom}
     \end{subfigure}
     \hfill
     \begin{subfigure}[b]{0.45\textwidth}
         \centering
         \includegraphics[width=\textwidth]{figs/graphs/ecdf_overhead.pdf}
         \caption{Comparison between the empirical cumulative distribution function (CDF) of the overhead (solid) and the CDF of the Lognormal model identified (dashed).}
         \label{fig:ecdf}
     \end{subfigure}
      \caption{Distribution of the job overheads due to \oar\ commissioning and decommissioning the nodes of the cluster. Figure \ref{fig:ecdf} shows the comparison between the data and the identified model.}
      \label{fig:overheads}
\end{figure}


#### Killing of Best-Effort Jobs

In \bat, when priority jobs are submitted, and they can be scheduled by killing best-effort jobs, the best-effort jobs are immediately stop, and the priority jobs started instantaneously.
In practice, the priority jobs spend some time in the waiting queue while the best-effort jobs are being killed and the nodes cleaned and set up.
This delay can be taking into account in the description of the priority jobs. 
The execution time in \bat\ must also contain this delay.



# Evaluation {#sec:eval}

In this Section, we evaluate the quality of the simulation.

## Experimental Protocol

For both the real system and the simulated one we will conduct the same scenario.
There are 500 \cigri\ jobs with an execution time of 235 seconds.
The submission loop of \cigri\ is called every 30 seconds in order to see how the system respond to delay in the control input.
After 2000 seconds, a priority job is submitted and takes half of the resources of the cluster for 1800 seconds. 
The controller of \cigri\ aims to regulate the quantity $w_k + r_k$ around the value 64 (which is the double of the number of resources in the cluster).


## Experimental Setup

The real experiments were carried on the `dahu` cluster of \grid\ \cite{grid5000} where the nodes have 2 Intel Xeon Gold 613 with 16 cores per CPU and 192 GiB of memory.
The reproducibility of the deployed environment is ensured by \nxc\ \cite{nxc}.

We deploy 3 nodes: one for the \oar\ server, one for \cigri, and one for the \oar\ cluster.
We do not deploy 32 nodes for the cluster, but instead deploy a single node and define 32 OAR resources.

## Execution time

One of the motivation of this study if the cost in time in resources of experiments.
Real experiments require deploying 3 resources (around 10 minutes), and then to execute the scenario (around 1h20 minutes).
In total, a single execution of the scenario consumes around 9 CPU hours.

In comparison, a simulation requires a single CPU, and needs 2 seconds to complete, thus consuming approximately $5.5\times 10^{-4}$ CPU hours

## Signals Comparison

For the simulation of \cigri\ to be useful, we need the signals of interest to have the same properties and behave the same in both simulation and real experiments.
The signals of interest are:

- the number of best-effort resources in the waiting queue

- the number of currently used resources on the cluster

- the dynamic of a \cigri\ submission (\ie\ the time it takes to see the impact of a submission) 

\begin{figure}
  \centering
  \includegraphics[width = 0.99\textwidth]{figs/graphs/batcigri.pdf}
  \caption{
Comparison of the signals of interest for the same experiment executed in simulation with \bat\ (red) and deploy (blue).
Signals appear to be in sync, but some amplitudes might differ.
}
  \label{fig:signals}
\end{figure}

Figure\ \ref{fig:signals} shows the comparison of the signals of interested between experiments of the same scenario executed in simulation (red) and deployed on real machines (blue).
The signals appear to be in sync.
The amplitude do differ, as can be observed around 500 seconds. 
The real system is obviously more sensible to noise.
This noise can be noticed when looking at the used resources (top left graph on Figure\ \ref{fig:signals}).
The cluster in the simulation is always full, whereas the cluster during real experiments is not (\eg\ at 1000, 2000, 4500 seconds).


## Gantt Charts Comparison

\begin{figure}
  \centering
  \includegraphics[width = 0.99\textwidth]{figs/graphs/gantts.pdf}
  \caption{
Comparison of the Gantt charts for the simulation (top) and real experiment (bottom) of the same scenario.
We observe a small lag, which is due to \oar, but both schedules are similar.
}
  \label{fig:gantts}
\end{figure}

Figure\ \ref{fig:gantts} compares the resulting Gantt charts of the experiment for the simulation (top) and real execution (bottom). 
We notice that there are "gaps" in the real schedule (\eg\ at time 1500 seconds on resource 24).
These gaps create a lag in the schedule which also impact the signals.

This lag comes from \oar\ scheduling algorithm.
Once the \oar\ decided to start to compute a scheduler, if any job arrives during the execution of the scheduler, those jobs will not be taken into account until the next schedule call.
Taking into account this lag in the simulation is complex, as \bat\ is responsible for the management of the simulation time, and because the time "stops" during the computation of the schedule.

# Conclusion

Distributed experiments are complex and costly.
Simulation techniques can help reduce the cost of such experiments.
However, simulators rely on models that can lose information compared to the real system.
In this paper, we implemented the essential behavior of \cigri, a grid middleware, in \bat.
Real experiments with \cigri\ requires 3 compute nodes for several hours, while simulation last a few seconds on a laptop.
We compared the behavior and similarities of signals of interest of our system in simulation and real experiments.
We had to modify the workload of the simulation to match the different overheads induced by the real system.
Results showed satisfying quality of signals in simulation. 

In this paper, we only focused on the execution time part of the jobs.
However, in our \cigri\ works \cite{guilloteau:icstcc, guilloteau:mfc} we control the submission of \cigri\ jobs to \oar\ in order regulate the load of a distributed file-system.
Taking into account a parallel file-system in \bat\ is feasible \cite{mercier_contribution_2019}.
However, the current limitation of \bat\ is the lack of probe mechanism to sense internal information and states.

<!---
(TODO: BATAR)
--->
